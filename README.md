# RGB and HEX Converter

This JavaScript module provides functions for converting colors between RGB and hexadecimal formats.


## Functions

### `rgb_to_hex(r, g, b)`

Converts RGB color values to hexadecimal format.

#### Parameters
- `r` (number): Red color component (0-255).
- `g` (number): Green color component (0-255).
- `b` (number): Blue color component (0-255).

#### Returns
- `string`: Hexadecimal representation of the color, e.g., "#00ff00" (green).

### `hex_to_rgb(hex)`

Converts hexadecimal color values to RGB format.

#### Parameters
- `hex` (string): Hexadecimal color format, e.g., "#00ff00" (green).

#### Returns
- `Object`: An object containing the RGB color components.
  - `r` (number): Red color component (0-255).
  - `g` (number): Green color component (0-255).
  - `b` (number): Blue color component (0-255).